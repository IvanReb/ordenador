public class Prueba {
  public static void main(String[] args) {
    Portatil p1 = new Portatil();
    p1.setCodigo("1415");
    p1.setPrecio(3500);
    p1.setSlogan("Ideal para sus viajes");
    p1.setPeso("2 kg");
    p1.mostrarDatos();
    Sobremesa s1 = new Sobremesa();
    s1.setCodigo("1516");
    s1.setPrecio(2800);
    s1.setSlogan("Es el que mas pesa, pero el que menos cuesta");
    s1.setDescripcion("Equipo sobremesa de tipo media torre");
    s1.mostrarDatos();
    System.out.println(p1.mostrarDatos());
    System.out.println(s1.mostrarDatos());
  }
}