public class Portatil extends Ordenador {
 private String peso;
 
 public Portatil(){
   super();
   peso = "";
 }

 public void setPeso(String peso){
   this.peso = peso;
 }

 public String mostrarDatos(){
   return "Ordenador portatil \n"+super.mostrarDatos().concat(", Peso: "+this.peso);
 }

}