public class Ordenador {
  private String codigo;
  private Integer precio;
  private String slogan;

  public Ordenador(){
    codigo="";
    precio = 0;
    slogan = "";
  }

  public void setCodigo(String codigo){
    this.codigo = codigo;
  }

  public void setPrecio(Integer precio){
    this.precio = precio;
  }

  public void setSlogan(String slogan){
    this.slogan = slogan;
  }

  public String mostrarDatos(){
    return "Codigo: "+this.codigo+", Precio: "+this.precio+", Slogan: "+this.slogan;
  }

}