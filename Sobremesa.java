public class Sobremesa extends Ordenador {

  private String descripcion;

  public Sobremesa(){
    super();
    descripcion = "";
  }

  public void setDescripcion(String descripcion){
    this.descripcion = descripcion;
  }

  public String mostrarDatos(){
    return "Ordenador sobremesa \n"+super.mostrarDatos().concat(", descripcion: "+this.descripcion);
  }

}